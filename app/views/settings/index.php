<div class="container">
    <div class="row">
        <?php include VIEW_DIR.'/settings/general.php'; ?>
        <?php include VIEW_DIR.'/settings/cc.php'; ?>
        <?php include VIEW_DIR.'/settings/cc_recurring.php'; ?>
        <?php include VIEW_DIR.'/settings/sepa.php'; ?>
        <?php include VIEW_DIR.'/settings/sepa_recurring.php'; ?>
        <?php include VIEW_DIR.'/settings/paypal.php'; ?>
    </div>
</div>

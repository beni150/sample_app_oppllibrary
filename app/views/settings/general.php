<div class="col-md-12">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">General</h3>
        </div>

        <div class="panel-body">
            <form action="<?php echo URL::to('settings', 'save'); ?>" class="form-horizontal" method="POST">
                <div class="form-group">
                    <label for="OPP_USER_ID" class="col-sm-2 control-label">User-ID</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="OPP_USER_ID" name="OPP_USER_ID" required value="<?php echo $settings->get('OPP_USER_ID'); ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="OPP_PASSWORD" class="col-sm-2 control-label">Password</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="OPP_PASSWORD" name="OPP_PASSWORD" required value="<?php echo $settings->get('OPP_PASSWORD'); ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label">Recurring?</label>

                    <div class="col-sm-10">
                        <div class="radio">
                            <label>
                                <input type="radio" name="OPP_RECURRING" value="1"<?php echo $settings->get('OPP_RECURRING') == 1 ? ' checked' : ''; ?>> Yes
                            </label>
                        </div>
                        <div class="radio">
                            <label>
                                <input type="radio" name="OPP_RECURRING" value="0"<?php echo $settings->get('OPP_RECURRING') == 0 ? ' checked' : ''; ?>> No
                            </label>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label for="OPP_MERCHANT_EMAIL" class="col-sm-2 control-label">Merchant Email</label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="OPP_MERCHANT_EMAIL" name="OPP_MERCHANT_EMAIL" required value="<?php echo $settings->get('OPP_MERCHANT_EMAIL'); ?>">
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-default">Save</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

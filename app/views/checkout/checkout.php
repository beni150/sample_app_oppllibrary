<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Checkout</h3>
                </div>

                <div class="panel-body">
                    <?php if ($recurringEnabled && ! empty($registrationIds)): ?>
                        <h3 align="center">Use stored payment data</h3>

                        <?php if ($isPaypal): ?>
                            <form action="<?php echo URL::to('checkout', 'process', array_merge(compact('payment'), $parameters)); ?>" class="form-horizontal" method="POST">
                                <div class="form-group" align="center">
                                    <div class="col-sm-12">
                                        <?php $i = 1; ?>
                                        <?php foreach ($registrationIds as $registration): ?>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio" name="id" value="<?php echo $registration['registration_id']; ?>"<?php echo $i === 1 ? ' checked' : ''; ?>> <?php echo $registration['email'].'<br>'.$registration['holder']; ?>
                                                </label>
                                            </div>

                                            <?php $i++; ?>
                                        <?php endforeach; ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-8 col-sm-10">
                                        <button type="submit" class="btn btn-default">Pay</button>
                                    </div>
                                </div>
                            </form>
                        <?php endif; ?>
                    <?php endif; ?>

                    <form action="<?php echo URL::to('checkout', 'process', array_merge(compact('payment'), $parameters)); ?>" class="paymentWidgets" method="POST" data-brands="<?php echo $brands; ?>">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="<?php echo $opp->copyAndPay()->paymentWidgetJs($checkoutId); ?>"></script>

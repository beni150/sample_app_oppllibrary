<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Checkout</h3>
                </div>

                <div class="panel-body">
                    <form action="<?php echo URL::to('checkout', 'checkout'); ?>" class="form-horizontal" method="POST">
                        <div class="form-group">
                            <label for="payment" class="col-sm-2 control-label">Payment Options</label>

                            <div class="col-sm-10">
                                <select class="form-control" id="payment" name="payment">
                                    <?php foreach ($paymentOptions as $key => $payment): ?>
                                        <option value="<?php echo $key; ?>"><?php echo $payment; ?></option>
                                    <?php endforeach; ?>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="amount" class="col-sm-2 control-label">Amount</label>

                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="amount" name="amount" required value="1" min="1">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="currency" class="col-sm-2 control-label">Currency</label>

                            <div class="col-sm-10">
                                <select class="form-control" id="currency" name="currency">
                                    <option value="EUR">EUR</option>
                                    <option value="USD">USD</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group" id="paymentTypeField">
                            <label for="paymentType" class="col-sm-2 control-label">Payment Type</label>

                            <div class="col-sm-10">
                                <select class="form-control" id="paymentType" name="paymentType">
                                    <option value="DB">Debit</option>
                                    <option value="PA">Pre-Authorization</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Continue</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#payment').change(function () {
            if (this.value === 'paypal') {
                $('#paymentType').val('DB');
                $('#paymentTypeField').hide();
            } else {
                $('#paymentTypeField').show();
            }
        });
    });
</script>

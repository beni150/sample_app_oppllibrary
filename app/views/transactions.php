<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Transactions</h3>
                </div>

                <div class="panel-body">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Method</th>
                                <th>Amount</th>
                                <th>Currency</th>
                                <th>Type</th>
                                <th width="30%">&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($transactions as $transaction): ?>
                                <tr>
                                    <td><?php echo $i; ?></td>
                                    <td><?php echo $transaction['method']; ?></td>
                                    <td><?php echo $transaction['amount']; ?></td>
                                    <td><?php echo $transaction['currency']; ?></td>
                                    <td><?php echo $transaction['type']; ?></td>
                                    <td>
                                        <?php if ($transaction['can_capture']): ?>
                                            <a href="<?php echo URL::to('transactions', 'capture', array('id' => $transaction['id'])); ?>" class="btn btn-primary">Capture</a>
                                        <?php endif; ?>

                                        <?php if ($transaction['can_refund']): ?>
                                            <a href="<?php echo URL::to('transactions', 'refund', array('id' => $transaction['id'])); ?>" class="btn btn-danger">Refund</a>
                                        <?php endif; ?>

                                        <?php if ($transaction['can_reverse']): ?>
                                            <a href="<?php echo URL::to('transactions', 'reverse', array('id' => $transaction['id'])); ?>" class="btn btn-warning">Reverse</a>
                                        <?php endif; ?>
                                    </td>
                                </tr>
                                <?php $i++; ?>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

namespace App\Models;

use DB;

class Transactions
{
    public static function all()
    {
        return DB::getInstance()->query('select * from transactions');
    }

    public static function find($id)
    {
        $result = DB::getInstance()->query("select * from transactions where id = {$id}");

        return $result->first();
    }
}

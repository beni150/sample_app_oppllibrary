<?php

namespace App\Controllers;

require_once APP_DIR.'/models/configurations.php';

use DB;
use URL;
use View;
use Request;
use Application;
use App\Models\Configurations;

class Settings
{
    public function index()
    {
        $cardTypes = Application::getInstance()->load('config')->get('opp.card_types');

        $transactionMode = Application::getInstance()->load('config')->get('opp.transaction_mode');

        $settings = Configurations::opp();

        return View::make('settings/index', compact('cardTypes', 'settings', 'transactionMode'), 'app');
    }

    public function save()
    {
        $data = Request::post();

        foreach ($data as $key => $value) {
            Configurations::set($key, $value);
        }

        header('Location: '.URL::to('settings'));
    }
}

<?php

namespace App\Controllers;

require_once APP_DIR.'/models/tokens.php';
require_once VENDOR_DIR.'/opp/src/Opp.php';
require_once APP_DIR.'/models/configurations.php';

use DB;
use View;
use Request;
use Payreto\Opp\Opp;
use App\Models\Tokens;
use InvalidArgumentException;
use App\Models\Configurations;

class Checkout
{
    public function index()
    {
        $paymentOptions = array(
            'cc' => 'Credit Card',
            'sepa' => 'SEPA',
            'paypal' => 'Paypal',
        );

        return View::make('checkout/index', compact('paymentOptions'), 'app');
    }

    public function checkout()
    {
        $recurringEnabled = Configurations::get('OPP_RECURRING');
        $payment = Request::post('payment');
        $isPaypal = $payment === 'paypal';
        $payments = array(
            'cc' => json_decode(Configurations::get('OPP_CREDIT_CARD_CARD_TYPES', '{}'), true),
            'sepa' => array('DIRECTDEBIT_SEPA'),
            'paypal' => array('PAYPAL'),
        );

        $opp = $this->setUpOppCheckout($payment);

        $parameters = array(
            'amount' => Request::post('amount'),
            'currency' => Request::post('currency'),
            'paymentType' => Request::post('paymentType'),
        );

        if ($recurringEnabled) {
            if ($isPaypal) {
                $result = json_decode($opp->copyAndPay()->prepareCheckoutWithTokenization($parameters, true), true);

                $registrationIds = Tokens::paypalTokens(1)->all();
            } else {
                // If user already has registration ID...
                if (DB::getInstance()->count("select count(*) as aggregate from tokens where user_id = 1 and payment = '{$payment}'") > 0) {
                    $parameters = array_merge($parameters, $registrationIds = Tokens::getRegistrationByUserId(1, $payment));
                }

                $result = json_decode($opp->copyAndPay()->prepareCheckoutWithTokenization($parameters), true);
            }
        } else {
            $result = json_decode($opp->copyAndPay()->prepareCheckout($parameters), true);
        }

        $checkoutId = $result['id'];
        $brands = implode(' ', $payments[$payment]);

        return View::make('checkout/checkout', compact('brands', 'checkoutId', 'opp', 'payment', 'parameters', 'recurringEnabled', 'isPaypal', 'registrationIds'), 'app');
    }

    protected function setUpOppCheckout($payment)
    {
        $authentication = array(
            'authentication.userId' => Configurations::get('OPP_USER_ID'),
            'authentication.password' => Configurations::get('OPP_PASSWORD'),
        );

        switch ($payment) {
            case 'cc':
                $authentication['authentication.entityId'] = Configurations::get('OPP_CREDIT_CARD_ENTITY_ID');
                $runningInProduction = Configurations::get('OPP_CREDIT_CARD_SERVER') == 1;
                break;
            case 'sepa':
                $authentication['authentication.entityId'] = Configurations::get('OPP_SEPA_ENTITY_ID');
                $runningInProduction = Configurations::get('OPP_SEPA_SERVER') == 1;
                break;
            case 'paypal':
                $authentication['authentication.entityId'] = Configurations::get('OPP_PAYPAL_ENTITY_ID');
                $runningInProduction = Configurations::get('OPP_PAYPAL_SERVER') == 1;
                break;
            default:
                throw new InvalidArgumentException("Invalid payment option [{$payment}]");
                break;
        }

        return new Opp($authentication, 'v1', $runningInProduction);
    }

    public function process()
    {
        $payment = Request::get('payment');
        $isPaypal = $payment === 'paypal';
        $id = Request::get('id') ?: Request::post('id');

        $opp = $this->setUpOppPaymentStatus($payment);

        if ($isPaypal) {
            $result = json_decode($opp->copyAndPay()->getRegistrationResult($id), true);

            if (isset($result['id'])) {
                $registrationId = $result['id'];
            } else {
                $registrationId = $id;
            }
        } else {
            $result = json_decode($opp->copyAndPay()->getPaymentStatus($id), true);

            $registrationId = $result['registrationId'];
        }

        if ($registrationId && ! Tokens::find(1, $registrationId)) {
            $email = 'NULL';
            $holder = 'NULL';

            if ($isPaypal) {
                $email = "'".$result['customer']['email']."'";
                $holder = "'".$result['customer']['givenName'].' '.$result['customer']['surname']."'";
            }

            DB::getInstance()->exec("insert into tokens (user_id, registration_id, payment, holder, email) values (1, '{$registrationId}', '{$payment}', {$holder}, {$email})");
        }

        $amount = Request::get('amount');
        $currency = Request::get('currency');
        $paymentType = Request::get('paymentType');

        if ($isPaypal) {
            $result = json_decode($opp->asyncServerToServer()->payRecurringly($registrationId, compact('amount', 'currency', 'paymentType')), true);
        }

        $paymentId = $result['id'];
        $canCaptureAndReverse = $paymentType === 'PA' ? 1 : 0;

        DB::getInstance()->exec("insert into transactions (user_id, payment_id, method, amount, currency, type, can_capture, can_reverse) values (1, '{$paymentId}', '{$payment}', '{$amount}', '{$currency}', '{$paymentType}', '{$canCaptureAndReverse}', '{$canCaptureAndReverse}')");

        return View::make('checkout/done', compact('result'), 'app');
    }

    protected function setUpOppPaymentStatus($payment)
    {
        $authentication = array(
            'authentication.userId' => Configurations::get('OPP_USER_ID'),
            'authentication.password' => Configurations::get('OPP_PASSWORD'),
        );

        switch ($payment) {
            case 'cc':
                $authentication['authentication.entityId'] = Configurations::get('OPP_CREDIT_CARD_ENTITY_ID');
                $runningInProduction = Configurations::get('OPP_CREDIT_CARD_SERVER') == 1;
                break;
            case 'sepa':
                $authentication['authentication.entityId'] = Configurations::get('OPP_SEPA_ENTITY_ID');
                $runningInProduction = Configurations::get('OPP_SEPA_SERVER') == 1;
                break;
            case 'paypal':
                $authentication['authentication.entityId'] = Configurations::get('OPP_PAYPAL_ENTITY_ID');
                $runningInProduction = Configurations::get('OPP_PAYPAL_SERVER') == 1;
                break;
            default:
                throw new InvalidArgumentException("Invalid payment option [{$payment}]");
                break;
        }

        return new Opp($authentication, 'v1', $runningInProduction);
    }
}

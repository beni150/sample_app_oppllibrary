<?php

namespace App\Controllers;

require_once VENDOR_DIR.'/opp/src/Opp.php';
require_once APP_DIR.'/models/transactions.php';
require_once APP_DIR.'/models/configurations.php';

use DB;
use URL;
use View;
use Request;
use Payreto\Opp\Opp;
use InvalidArgumentException;
use App\Models\Configurations;
use App\Models\Transactions as TransactionsModel;

class Transactions
{
    public function index()
    {
        $i = 1;
        $transactions = TransactionsModel::all();

        return View::make('transactions', compact('transactions', 'i'), 'app');
    }

    public function capture()
    {
        $id = Request::get('id');
        $transaction = TransactionsModel::find($id);

        $opp = $this->setUpOpp($transaction['method']);

        $opp->backoffice()->capture($transaction['payment_id'], array(
            'amount' => $transaction['amount'],
            'currency' => $transaction['currency'],
        ));

        DB::getInstance()->exec("update transactions set can_capture = 0, can_refund = 1, can_reverse = 0 where id = {$id}");

        header('Location: '.URL::to('transactions'));
    }

    public function refund()
    {
        $id = Request::get('id');
        $transaction = TransactionsModel::find($id);

        $opp = $this->setUpOpp($transaction['method']);

        $opp->backoffice()->refund($transaction['payment_id'], array(
            'amount' => $transaction['amount'],
            'currency' => $transaction['currency'],
        ));

        DB::getInstance()->exec("update transactions set can_capture = 0, can_refund = 0, can_reverse = 0 where id = {$id}");

        header('Location: '.URL::to('transactions'));
    }

    public function reverse()
    {
        $id = Request::get('id');
        $transaction = TransactionsModel::find($id);

        $opp = $this->setUpOpp($transaction['method']);

        $opp->backoffice()->reversal($transaction['payment_id']);

        DB::getInstance()->exec("update transactions set can_capture = 0, can_refund = 0, can_reverse = 0 where id = {$id}");

        header('Location: '.URL::to('transactions'));
    }

    protected function setUpOpp($method)
    {
        $authentication = array(
            'authentication.userId' => Configurations::get('OPP_USER_ID'),
            'authentication.password' => Configurations::get('OPP_PASSWORD'),
        );

        switch ($method) {
            case 'cc':
                $authentication['authentication.entityId'] = Configurations::get('OPP_CREDIT_CARD_ENTITY_ID');
                $runningInProduction = Configurations::get('OPP_CREDIT_CARD_SERVER') == 1;
                break;
            case 'sepa':
                $authentication['authentication.entityId'] = Configurations::get('OPP_SEPA_ENTITY_ID');
                $runningInProduction = Configurations::get('OPP_SEPA_SERVER') == 1;
                break;
            case 'paypal':
                $authentication['authentication.entityId'] = Configurations::get('OPP_PAYPAL_ENTITY_ID');
                $runningInProduction = Configurations::get('OPP_PAYPAL_SERVER') == 1;
                break;
            default:
                throw new InvalidArgumentException("Invalid payment method [{$method}]");
                break;
        }

        return new Opp($authentication, 'v1', $runningInProduction);
    }
}

<?php

namespace App\Controllers;

use View;

class Home
{
    public function index()
    {
        return View::make('home', array(), 'app');
    }
}

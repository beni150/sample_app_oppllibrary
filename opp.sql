-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 26, 2017 at 05:29 PM
-- Server version: 5.7.19-0ubuntu0.16.04.1
-- PHP Version: 7.1.7-1+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `opp`
--

-- --------------------------------------------------------

--
-- Table structure for table `configurations`
--

CREATE TABLE `configurations` (
  `name` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `configurations`
--

INSERT INTO `configurations` (`name`, `value`) VALUES
('OPP_CREDIT_CARDR_CARD_TYPES', '["VISA","MASTER","AMEX","DINERS","JCB"]'),
('OPP_CREDIT_CARDR_ENABLED', '1'),
('OPP_CREDIT_CARDR_ENTITY_ID', '8a8294174b7ecb28014b9699220015ca'),
('OPP_CREDIT_CARDR_SERVER', '0'),
('OPP_CREDIT_CARDR_TRANSACTION_MODE', 'DB'),
('OPP_CREDIT_CARD_CARD_TYPES', '["VISA","MASTER","AMEX","DINERS","JCB"]'),
('OPP_CREDIT_CARD_ENABLED', '1'),
('OPP_CREDIT_CARD_ENTITY_ID', '8a8294174d0a8edd014d0abf32d201ac'),
('OPP_CREDIT_CARD_SERVER', '0'),
('OPP_CREDIT_CARD_TRANSACTION_MODE', 'DB'),
('OPP_MERCHANT_EMAIL', 'pluginsales@payreto.eu'),
('OPP_PASSWORD', 'jGNhYaX5'),
('OPP_PAYPAL_ENABLED', '1'),
('OPP_PAYPAL_ENTITY_ID', '8a8294174d0a8edd014d0acb7ceb021c'),
('OPP_PAYPAL_SERVER', '0'),
('OPP_RECURRING', '1'),
('OPP_SEPAR_ENABLED', '1'),
('OPP_SEPAR_ENTITY_ID', '8a8294174b7ecb28014b9699220015ca'),
('OPP_SEPAR_SERVER', '0'),
('OPP_SEPAR_TRANSACTION_MODE', 'DB'),
('OPP_SEPA_ENABLED', '1'),
('OPP_SEPA_ENTITY_ID', '8a8294174d0a8edd014d0ac82bbd01ee'),
('OPP_SEPA_SERVER', '0'),
('OPP_SEPA_TRANSACTION_MODE', 'DB'),
('OPP_USER_ID', '8a8294174d0a8edd014d0a9f9743007b');

-- --------------------------------------------------------

--
-- Table structure for table `tokens`
--

CREATE TABLE `tokens` (
  `user_id` int(11) NOT NULL,
  `registration_id` varchar(255) NOT NULL,
  `payment` varchar(255) NOT NULL,
  `holder` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tokens`
--

INSERT INTO `tokens` (`user_id`, `registration_id`, `payment`, `holder`, `email`) VALUES
(1, '8a8294495d791abd015d7e4c08002706', 'cc', NULL, NULL),
(1, '8a8294495d791abd015d7e5062452bc8', 'sepa', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment_id` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `amount` int(11) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `can_capture` int(11) NOT NULL DEFAULT '0',
  `can_refund` int(11) NOT NULL DEFAULT '0',
  `can_reverse` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `payment_id`, `method`, `amount`, `currency`, `type`, `can_capture`, `can_refund`, `can_reverse`) VALUES
(1, 1, '8a82944a5d79314e015d7e4c08e71fc7', 'cc', 1, 'EUR', 'DB', 0, 0, 0),
(2, 1, '8a82944a5d79314e015d7e4d49e220a4', 'cc', 1, 'EUR', 'PA', 0, 0, 0),
(3, 1, '8a82944a5d79314e015d7e5063a723ec', 'sepa', 1, 'EUR', 'DB', 0, 0, 0),
(4, 1, '8a82944a5d79314e015d7e512598246e', 'sepa', 1, 'EUR', 'PA', 0, 0, 0),
(5, 1, '8a82944a5d79314e015d7e6c4bc7461e', 'cc', 1, 'EUR', 'PA', 0, 0, 0),
(6, 1, '8a8294495d791abd015d7e6cd30d4e28', 'sepa', 1, 'EUR', 'PA', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`) VALUES
(1, 'Dexter Morar');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `configurations`
--
ALTER TABLE `configurations`
  ADD PRIMARY KEY (`name`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `tokens`
--
ALTER TABLE `tokens`
  ADD KEY `user_id` (`user_id`),
  ADD KEY `registration_id` (`registration_id`);

--
-- Indexes for table `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

<?php

require_once __DIR__.'/system/application.php';

$app = System\Application::getInstance();

$response = $app->handle();

$response->send();

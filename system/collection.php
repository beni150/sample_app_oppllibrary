<?php

namespace System;

use ArrayIterator;
use IteratorAggregate;

class Collection implements IteratorAggregate
{
    protected $items = array();

    public function __construct($items = array())
    {
        $this->items = $items;
    }

    public function all()
    {
        return $this->items;
    }

    public function first()
    {
        return $this->items[0];
    }

    public function get($key, $default = null)
    {
        return $this->has($key)
                ? $this->items[$key]
                : $default;
    }

    public function has($key)
    {
        return array_key_exists($key, $this->items);
    }

    public function isEmpty()
    {
        return empty($this->items);
    }

    public function getIterator()
    {
        return new ArrayIterator($this->items);
    }

    public function __toString()
    {
        return json_encode($this->items);
    }
}

<?php

namespace System;

require_once __DIR__.'/db.php';
require_once __DIR__.'/url.php';
require_once __DIR__.'/config.php';
require_once __DIR__.'/request.php';
require_once __DIR__.'/response.php';

use Exception;
use InvalidArgumentException;

final class Application
{
    protected static $instance;

    protected $services = array();

    public static function getInstance()
    {
        return static::$instance instanceof static
            ? static::$instance
            : static::$instance = new static;
    }

    protected function __construct()
    {
        $this->defineConstant();
        $this->bootConfig();
        $this->registerAliases();
    }

    protected function bootConfig()
    {
        $this->services['config'] = new Config(CONFIG_DIR);
    }

    protected function registerAliases()
    {
        $aliases = array(
            'System\\DB' => 'DB',
            'System\\URL' => 'URL',
            'System\\View' => 'View',
            'System\\Request' => 'Request',
            'System\\Application' => 'Application',
        );

        foreach ($aliases as $original => $alias) {
            class_alias($original, $alias);
        }
    }

    protected function defineConstant()
    {
        if (! defined('SYSTEM_DIR')) {
            define('SYSTEM_DIR', __DIR__);
        }

        if (! defined('APP_DIR')) {
            define('APP_DIR', SYSTEM_DIR.'/../app');
        }

        if (! defined('VIEW_DIR')) {
            define('VIEW_DIR', APP_DIR.'/views');
        }

        if (! defined('CONFIG_DIR')) {
            define('CONFIG_DIR', SYSTEM_DIR.'/../config');
        }

        if (! defined('STORAGE_DIR')) {
            define('STORAGE_DIR', SYSTEM_DIR.'/../storage');
        }

        if (! defined('VENDOR_DIR')) {
            define('VENDOR_DIR', SYSTEM_DIR.'/../vendor');
        }
    }

    public function load($service)
    {
        if (isset($this->services[$service])) {
            return $this->services[$service];
        }

        throw new InvalidArgumentException("Service not found [{$service}]");
    }

    public function handle()
    {
        try {
            $controller = $this->getController();

            $method = $this->getMethod($controller);

            return new Response(call_user_func(array($controller, $method)));
        } catch (Exception $e) {
            return new Response($this->parseError($e));
        }
    }

    protected function getController()
    {
        $controller = Request::get('c') ?: $this->load('config')->get('app.default_controller');

        $controllerFile = APP_DIR.'/controllers/'.$controller.'.php';

        if (file_exists($controllerFile)) {
            require_once $controllerFile;
        } else {
            throw new InvalidArgumentException("Controller not exists [{$controller}]");
        }

        $controller = 'App\\Controllers\\'.ucfirst($controller);

        if (! class_exists($controller)) {
            throw new InvalidArgumentException("Controller class not exists [{$controller}]");
        }

        return new $controller;
    }

    protected function getMethod($controller)
    {
        $method = Request::get('m') ?: 'index';

        if (method_exists($controller, $method)) {
            return $method;
        }

        $class = get_class($controller);
        throw new InvalidArgumentException("Method not exists [{$method}] in {$class} class");
    }

    protected function parseError($error)
    {
        return nl2br(get_class($error).' : '.$error->getMessage().PHP_EOL.$error->getTraceAsString());
    }
}

<?php

namespace System;

require_once __DIR__.'/collection.php';
require_once __DIR__.'/application.php';

use PDO;

class DB
{
    protected static $instance;

    protected $pdo;

    protected function __construct()
    {
        if (! $this->pdo instanceof PDO) {
            $this->pdo = $this->createPDOConnection();
        }
    }

    protected function createPDOConnection()
    {
        $config = Application::getInstance()->load('config');

        $dsn = sprintf(
            'mysql:dbname=%s;host=%s;port=%s',
            $config->get('db.database'),
            $config->get('db.host'),
            $config->get('db.port')
        );

        return new PDO($dsn, $config->get('db.username'), $config->get('db.password'));
    }

    public static function getInstance()
    {
        return static::$instance instanceof static
            ? static::$instance
            : static::$instance = new static;
    }

    public function query($query, $parameters = array())
    {
        $statement = $this->pdo->prepare($query);

        $statement->execute($parameters);

        return new Collection($statement->fetchAll(PDO::FETCH_ASSOC));
    }

    public function exec($query)
    {
        return $this->pdo->exec($query);
    }

    public function count($query, $parameters = array())
    {
        $result = $this->query($query, $parameters)->first();

        return $result['aggregate'];
    }
}

<?php

namespace System;

require_once __DIR__.'/application.php';

class URL
{
    public static function to($controller, $method = 'index', $query = array())
    {
        $baseUrl = Application::getInstance()->load('config')->get('app.url');

        $queryString = http_build_query(array_merge($query, array(
            'm' => $method, 'c' => $controller,
        )));

        return sprintf(
            '%s/index.php?%s', rtrim($baseUrl, '/'), $queryString
        );
    }
}

<?php

namespace System;

class Config
{
    protected $items = array();

    public function __construct($dir)
    {
        $files = array_slice(scandir($dir), 2);

        foreach ($files as $file) {
            $key = str_replace(substr($file, -4), '', $file);

            $this->items[$key] = require "{$dir}/{$file}";
        }
    }

    public function get($item, $default = null)
    {
        if ($this->has($item)) {
            list($config, $key) = explode('.', $item);

            return $this->items[$config][$key];
        }

        return $default;
    }

    public function has($item)
    {
        list($config, $key) = explode('.', $item);

        return array_key_exists($config, $this->items) && array_key_exists($key, $this->items[$config]);
    }
}

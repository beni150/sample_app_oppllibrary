<?php

return array(
    'card_types' => array(
        'VISA' => 'Visa',
        'MASTER' => 'Mastercard',
        'AMEX' => 'American Express',
        'DINERS' => 'Diners',
        'JCB' => 'JCB',
    ),
    'transaction_mode' => array(
        'DB' => 'Debit',
        'PA' => 'Pre-Authorization',
    ),
);
